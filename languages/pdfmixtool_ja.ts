<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ja">
<context>
    <name>AboutDialog</name>
    <message>
        <location filename="../src/aboutdialog.cpp" line="33"/>
        <source>About PDF Mix Tool</source>
        <translation>PDF Mix Tool について</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="44"/>
        <source>Close</source>
        <translation>閉じる</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="55"/>
        <source>Version %1</source>
        <translation>バージョン %1</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="67"/>
        <source>An application to split, merge, rotate and mix PDF files.</source>
        <translation>PDF ファイルを分割、結合、回転、ミックスするためのアプリケーションです。</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="70"/>
        <source>Website</source>
        <translation>ウェブサイト</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="77"/>
        <source>About</source>
        <translation>このアプリケーションについて</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="94"/>
        <source>Authors</source>
        <translation>作者</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="96"/>
        <source>Translators</source>
        <translation>翻訳者</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="105"/>
        <source>Credits</source>
        <translation>クレジット</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="129"/>
        <source>License</source>
        <translation>ライセンス</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="142"/>
        <source>Submit a pull request</source>
        <translation>プルリクエストを出す</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="143"/>
        <source>Report a bug</source>
        <translation>バグを報告する</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="144"/>
        <source>Help translating</source>
        <translation>翻訳を手伝う</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="151"/>
        <source>Contribute</source>
        <translation>貢献</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="168"/>
        <source>Changelog</source>
        <translation>更新履歴</translation>
    </message>
</context>
<context>
    <name>AddEmptyPages</name>
    <message>
        <location filename="../src/single_file_operations/add_empty_pages.cpp" line="40"/>
        <source>Count:</source>
        <translation>数:</translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/add_empty_pages.cpp" line="44"/>
        <source>Page size</source>
        <translation>ページサイズ</translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/add_empty_pages.cpp" line="47"/>
        <source>Same as document</source>
        <translation>ドキュメントと同じ</translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/add_empty_pages.cpp" line="50"/>
        <source>Custom:</source>
        <translation>カスタム:</translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/add_empty_pages.cpp" line="75"/>
        <source>Standard:</source>
        <translation>標準:</translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/add_empty_pages.cpp" line="84"/>
        <source>Portrait</source>
        <translation>縦向き</translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/add_empty_pages.cpp" line="86"/>
        <source>Landscape</source>
        <translation>横向き</translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/add_empty_pages.cpp" line="89"/>
        <source>Location</source>
        <translation>位置</translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/add_empty_pages.cpp" line="92"/>
        <source>Before</source>
        <translation>前</translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/add_empty_pages.cpp" line="94"/>
        <source>After</source>
        <translation>後</translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/add_empty_pages.cpp" line="97"/>
        <source>Page:</source>
        <translation>ページ:</translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/add_empty_pages.cpp" line="111"/>
        <source>Save</source>
        <translation>保存</translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/add_empty_pages.cpp" line="122"/>
        <source>Save as…</source>
        <translation>名前を付けて保存…</translation>
    </message>
</context>
<context>
    <name>Booklet</name>
    <message>
        <location filename="../src/single_file_operations/booklet.cpp" line="34"/>
        <source>Left</source>
        <translation>左</translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/booklet.cpp" line="35"/>
        <source>Right</source>
        <translation>右</translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/booklet.cpp" line="36"/>
        <source>Binding:</source>
        <translation>装丁:</translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/booklet.cpp" line="41"/>
        <source>Generate booklet</source>
        <translation>ブックレットを生成</translation>
    </message>
</context>
<context>
    <name>DeletePages</name>
    <message>
        <location filename="../src/single_file_operations/delete_pages.cpp" line="41"/>
        <source>Delete pages:</source>
        <translation>ページを削除:</translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/delete_pages.cpp" line="43"/>
        <source>Delete even pages</source>
        <translation>偶数ページを削除</translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/delete_pages.cpp" line="45"/>
        <source>Delete odd pages</source>
        <translation>奇数ページを削除</translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/delete_pages.cpp" line="61"/>
        <source>Save</source>
        <translation>保存</translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/delete_pages.cpp" line="72"/>
        <source>Save as…</source>
        <translation>名前を付けて保存…</translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/delete_pages.cpp" line="153"/>
        <source>&lt;p&gt;Pages to be deleted are badly formatted. Please make sure you complied with the following rules:&lt;/p&gt;&lt;ul&gt;&lt;li&gt;intervals of pages must be written indicating the first page and the last page separated by a dash (e.g. &quot;1-5&quot;);&lt;/li&gt;&lt;li&gt;single pages and intervals of pages must be separated by spaces, commas or both (e.g. &quot;1, 2, 3, 5-10&quot; or &quot;1 2 3 5-10&quot;);&lt;/li&gt;&lt;li&gt;all pages and intervals of pages must be between 1 and the number of pages of the PDF file;&lt;/li&gt;&lt;li&gt;only numbers, spaces, commas and dashes can be used. All other characters are not allowed.&lt;/li&gt;&lt;/ul&gt;</source>
        <translation>&lt;p&gt;削除するページが適切な形式ではありません。次の規則に従っていることを確認してください:&lt;/p&gt;&lt;ul&gt;&lt;li&gt;ページの間隔は、最初のページと最後のページをダッシュで区切って (例えば &quot;1-5&quot; ) 書く必要があります&lt;/li&gt;&lt;li&gt;単一のページとページ間隔は、スペース、カンマ、またはその両方で (例えば &quot;1, 2, 3, 5-10&quot; や &quot;1 2 3 5-10&quot;) 区切る必要があります。&lt;/li&gt;&lt;li&gt;すべてのページとページ間隔は、1 から PDF ファイルのページ数の間でなければなりません。&lt;/li&gt;&lt;li&gt;数字、スペース、カンマ、ダッシュのみ使用できます。他の文字は使用できません&lt;/li&gt;&lt;/ul&gt;</translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/delete_pages.cpp" line="168"/>
        <source>Error</source>
        <translation>エラー</translation>
    </message>
</context>
<context>
    <name>EditMultipageProfileDialog</name>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="29"/>
        <source>Edit multipage profile</source>
        <translation>マルチページプロファイルを編集</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="64"/>
        <location filename="../src/editmultipageprofiledialog.cpp" line="166"/>
        <source>Left</source>
        <translation>左</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="65"/>
        <location filename="../src/editmultipageprofiledialog.cpp" line="69"/>
        <source>Center</source>
        <translation>中央</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="66"/>
        <location filename="../src/editmultipageprofiledialog.cpp" line="168"/>
        <source>Right</source>
        <translation>右</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="68"/>
        <location filename="../src/editmultipageprofiledialog.cpp" line="170"/>
        <source>Top</source>
        <translation>上</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="70"/>
        <location filename="../src/editmultipageprofiledialog.cpp" line="172"/>
        <source>Bottom</source>
        <translation>下</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="101"/>
        <source>Name:</source>
        <translation>名前:</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="109"/>
        <source>Output page size</source>
        <translation>出力ページサイズ</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="115"/>
        <source>Custom size:</source>
        <translation>カスタムサイズ:</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="129"/>
        <source>Pages layout</source>
        <translation>ページレイアウト</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="132"/>
        <source>Rows:</source>
        <translation>行:</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="135"/>
        <source>Columns:</source>
        <translation>列:</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="138"/>
        <source>Rotation:</source>
        <translation>回転:</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="141"/>
        <source>Spacing:</source>
        <translation>間隔:</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="149"/>
        <source>Pages alignment</source>
        <translation>ページ配置</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="152"/>
        <source>Horizontal:</source>
        <translation>横:</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="155"/>
        <source>Vertical:</source>
        <translation>縦:</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="163"/>
        <source>Margins</source>
        <translation>余白</translation>
    </message>
</context>
<context>
    <name>EditPageLayout</name>
    <message>
        <location filename="../src/single_file_operations/edit_page_layout.cpp" line="49"/>
        <source>No rotation</source>
        <translation>回転なし</translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/edit_page_layout.cpp" line="56"/>
        <source>Rotation:</source>
        <translation>回転:</translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/edit_page_layout.cpp" line="58"/>
        <location filename="../src/single_file_operations/edit_page_layout.cpp" line="121"/>
        <location filename="../src/single_file_operations/edit_page_layout.cpp" line="208"/>
        <source>Disabled</source>
        <translation>無効</translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/edit_page_layout.cpp" line="66"/>
        <location filename="../src/single_file_operations/edit_page_layout.cpp" line="133"/>
        <location filename="../src/single_file_operations/edit_page_layout.cpp" line="216"/>
        <source>New custom profile…</source>
        <translation>新しいカスタムプロファイル…</translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/edit_page_layout.cpp" line="72"/>
        <source>Multipage:</source>
        <translation>マルチページ:</translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/edit_page_layout.cpp" line="80"/>
        <source>Scale page:</source>
        <translation>ページの倍率:</translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/edit_page_layout.cpp" line="88"/>
        <source>Save</source>
        <translation>保存</translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/edit_page_layout.cpp" line="101"/>
        <source>Save as…</source>
        <translation>名前を付けて保存…</translation>
    </message>
</context>
<context>
    <name>EditPdfEntryDialog</name>
    <message>
        <location filename="../src/editpdfentrydialog.cpp" line="37"/>
        <source>Edit PDF files&apos; properties</source>
        <translation>PDF ファイルのプロパティを編集</translation>
    </message>
    <message>
        <location filename="../src/editpdfentrydialog.cpp" line="40"/>
        <source>No rotation</source>
        <translation>回転なし</translation>
    </message>
    <message>
        <location filename="../src/editpdfentrydialog.cpp" line="45"/>
        <source>Disabled</source>
        <translation>無効</translation>
    </message>
    <message>
        <location filename="../src/editpdfentrydialog.cpp" line="90"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../src/editpdfentrydialog.cpp" line="96"/>
        <source>Cancel</source>
        <translation>キャンセル</translation>
    </message>
    <message>
        <location filename="../src/editpdfentrydialog.cpp" line="103"/>
        <source>Multipage:</source>
        <translation>マルチページ:</translation>
    </message>
    <message>
        <location filename="../src/editpdfentrydialog.cpp" line="105"/>
        <source>Rotation:</source>
        <translation>回転:</translation>
    </message>
</context>
<context>
    <name>ExtractPages</name>
    <message>
        <location filename="../src/single_file_operations/extract_pages.cpp" line="44"/>
        <source>Extract pages:</source>
        <translation>ページを抽出:</translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/extract_pages.cpp" line="47"/>
        <source>Extract all pages</source>
        <translation>すべてのページを抽出</translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/extract_pages.cpp" line="50"/>
        <source>Extract even pages</source>
        <translation>偶数ページを抽出</translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/extract_pages.cpp" line="53"/>
        <source>Extract odd pages</source>
        <translation>奇数ページを抽出</translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/extract_pages.cpp" line="60"/>
        <source>Output PDF base name:</source>
        <translation>出力する PDF の基本名:</translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/extract_pages.cpp" line="73"/>
        <source>Extract to individual PDF files</source>
        <translation>個別の PDF ファイルに抽出</translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/extract_pages.cpp" line="77"/>
        <source>Extract to single PDF</source>
        <translation>単一の PDF に抽出</translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/extract_pages.cpp" line="145"/>
        <source>&lt;p&gt;Pages to be extracted are badly formatted. Please make sure you complied with the following rules:&lt;/p&gt;&lt;ul&gt;&lt;li&gt;intervals of pages must be written indicating the first page and the last page separated by a dash (e.g. &quot;1-5&quot;);&lt;/li&gt;&lt;li&gt;single pages and intervals of pages must be separated by spaces, commas or both (e.g. &quot;1, 2, 3, 5-10&quot; or &quot;1 2 3 5-10&quot;);&lt;/li&gt;&lt;li&gt;all pages and intervals of pages must be between 1 and the number of pages of the PDF file;&lt;/li&gt;&lt;li&gt;only numbers, spaces, commas and dashes can be used. All other characters are not allowed.&lt;/li&gt;&lt;/ul&gt;</source>
        <translation>&lt;p&gt;抽出するページが適切な形式ではありません。次の規則に従っていることを確認してください:&lt;/p&gt;&lt;ul&gt;&lt;li&gt;ページの間隔は、最初のページと最後のページをダッシュで区切って (例えば &quot;1-5&quot; ) 書く必要があります&lt;/li&gt;&lt;li&gt;単一のページとページ間隔は、スペース、カンマ、またはその両方で (例えば &quot;1, 2, 3, 5-10&quot; や &quot;1 2 3 5-10&quot;) 区切る必要があります。&lt;/li&gt;&lt;li&gt;すべてのページとページ間隔は、1 から PDF ファイルのページ数の間でなければなりません。&lt;/li&gt;&lt;li&gt;数字、スペース、カンマ、ダッシュのみ使用できます。他の文字は使用できません&lt;/li&gt;&lt;/ul&gt;</translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/extract_pages.cpp" line="160"/>
        <source>Error</source>
        <translation>エラー</translation>
    </message>
</context>
<context>
    <name>InputPdfFileDelegate</name>
    <message>
        <location filename="../src/inputpdffiledelegate.cpp" line="113"/>
        <source>All</source>
        <translation>すべて</translation>
    </message>
    <message>
        <location filename="../src/inputpdffiledelegate.cpp" line="80"/>
        <source>Page order:</source>
        <translation>ページの順序:</translation>
    </message>
    <message>
        <location filename="../src/inputpdffiledelegate.cpp" line="83"/>
        <source>reverse</source>
        <translation>逆順</translation>
    </message>
    <message>
        <location filename="../src/inputpdffiledelegate.cpp" line="85"/>
        <source>forward</source>
        <translation>通常</translation>
    </message>
    <message>
        <location filename="../src/inputpdffiledelegate.cpp" line="126"/>
        <source>Pages:</source>
        <translation>ページ:</translation>
    </message>
    <message>
        <location filename="../src/inputpdffiledelegate.cpp" line="129"/>
        <source>Multipage:</source>
        <translation>マルチページ:</translation>
    </message>
    <message>
        <location filename="../src/inputpdffiledelegate.cpp" line="134"/>
        <source>Disabled</source>
        <translation>無効</translation>
    </message>
    <message>
        <location filename="../src/inputpdffiledelegate.cpp" line="137"/>
        <source>Rotation:</source>
        <translation>回転:</translation>
    </message>
    <message>
        <location filename="../src/inputpdffiledelegate.cpp" line="140"/>
        <source>Outline entry:</source>
        <translation>アウトラインエントリー:</translation>
    </message>
</context>
<context>
    <name>InputPdfFileWidget</name>
    <message>
        <location filename="../src/inputpdffilewidget.cpp" line="57"/>
        <source>Reverse page order:</source>
        <translation>ページ順序を反転:</translation>
    </message>
    <message>
        <location filename="../src/inputpdffilewidget.cpp" line="71"/>
        <source>Disabled</source>
        <translation>無効</translation>
    </message>
    <message>
        <location filename="../src/inputpdffilewidget.cpp" line="79"/>
        <source>New custom profile…</source>
        <translation>新しいカスタムプロファイル…</translation>
    </message>
    <message>
        <location filename="../src/inputpdffilewidget.cpp" line="81"/>
        <source>No rotation</source>
        <translation>回転なし</translation>
    </message>
    <message>
        <location filename="../src/inputpdffilewidget.cpp" line="86"/>
        <source>Pages:</source>
        <translation>ページ:</translation>
    </message>
    <message>
        <location filename="../src/inputpdffilewidget.cpp" line="88"/>
        <source>Multipage:</source>
        <translation>マルチページ:</translation>
    </message>
    <message>
        <location filename="../src/inputpdffilewidget.cpp" line="90"/>
        <source>Rotation:</source>
        <translation>回転:</translation>
    </message>
    <message>
        <location filename="../src/inputpdffilewidget.cpp" line="92"/>
        <source>Outline entry:</source>
        <translation>アウトラインエントリー:</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../src/mainwindow.cpp" line="173"/>
        <source>Add PDF file</source>
        <translation>PDF ファイルを追加</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="178"/>
        <source>Move up</source>
        <translation>上に移動</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="183"/>
        <source>Move down</source>
        <translation>下に移動</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="188"/>
        <source>Remove file</source>
        <translation>ファイルを削除</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="126"/>
        <source>About</source>
        <translation>このアプリケーションについて</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="221"/>
        <location filename="../src/mainwindow.cpp" line="225"/>
        <source>Generate PDF</source>
        <translation>PDF を生成</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="307"/>
        <source>Edit page layout</source>
        <translation>ページレイアウトを編集</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="628"/>
        <source>PDF generation error</source>
        <translation>PDF 生成エラー</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="376"/>
        <source>Select one or more PDF files to open</source>
        <translation>開く PDF ファイルを一つまたは複数選択してください</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="162"/>
        <source>Edit</source>
        <translation>編集</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="163"/>
        <source>View</source>
        <translation>表示</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="166"/>
        <source>Main toolbar</source>
        <translation>メインツールバー</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="116"/>
        <source>Menu</source>
        <translation>メニュー</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="97"/>
        <source>Multiple files</source>
        <translation>複数のファイル</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="98"/>
        <source>Single file</source>
        <translation>単一のファイル</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="121"/>
        <source>Multipage profiles…</source>
        <translation>マルチページプロファイル…</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="131"/>
        <source>Exit</source>
        <translation>終了</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="278"/>
        <source>Open PDF file…</source>
        <translation>PDF ファイルを開く…</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="302"/>
        <source>Booklet</source>
        <translation>ブックレット</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="784"/>
        <source>Always overwrite</source>
        <translation>常に上書き</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="939"/>
        <source>Select save directory</source>
        <translation>保存ディレクトリを選択</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1002"/>
        <source>Extract to single PDF</source>
        <translation>単一の PDF に抽出</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="329"/>
        <source>Add empty pages</source>
        <translation>空のページを追加</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="336"/>
        <source>Delete pages</source>
        <translation>ページを削除</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="343"/>
        <source>Extract pages</source>
        <translation>ページを抽出</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="378"/>
        <location filename="../src/mainwindow.cpp" line="639"/>
        <location filename="../src/mainwindow.cpp" line="705"/>
        <location filename="../src/mainwindow.cpp" line="742"/>
        <location filename="../src/mainwindow.cpp" line="807"/>
        <location filename="../src/mainwindow.cpp" line="1006"/>
        <source>PDF files (*.pdf)</source>
        <translation>PDF ファイル (*.pdf)</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="602"/>
        <source>Output pages: %1</source>
        <translation>出力ページ: %1</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="703"/>
        <source>Select a PDF file</source>
        <translation>PDF ファイルを選択</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="738"/>
        <source>Save booklet PDF file</source>
        <translation>ブックレット PDF ファイルを保存</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="777"/>
        <source>Overwrite File?</source>
        <translation>ファイルを上書きしますか？</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="778"/>
        <source>A file called «%1» already exists. Do you want to overwrite it?</source>
        <translation>ファイル «%1» はすでに存在します。上書きしますか？</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="611"/>
        <source>&lt;p&gt;Output pages of file &lt;b&gt;%1&lt;/b&gt; are badly formatted. Please make sure you complied with the following rules:&lt;/p&gt;&lt;ul&gt;&lt;li&gt;intervals of pages must be written indicating the first page and the last page separated by a dash (e.g. &quot;1-5&quot;);&lt;/li&gt;&lt;li&gt;single pages and intervals of pages must be separated by spaces, commas or both (e.g. &quot;1, 2, 3, 5-10&quot; or &quot;1 2 3 5-10&quot;);&lt;/li&gt;&lt;li&gt;all pages and intervals of pages must be between 1 and the number of pages of the PDF file;&lt;/li&gt;&lt;li&gt;only numbers, spaces, commas and dashes can be used. All other characters are not allowed.&lt;/li&gt;&lt;/ul&gt;</source>
        <translation>&lt;p&gt;ファイルの出力ページ &lt;b&gt;%1&lt;/b&gt; が適切な形式ではありません。次の規則に従っていることを確認してください:&lt;/p&gt;&lt;ul&gt;&lt;li&gt;ページの間隔は、最初のページと最後のページをダッシュで区切って (例えば &quot;1-5&quot; ) 書く必要があります&lt;/li&gt;&lt;li&gt;単一のページとページ間隔は、スペース、カンマ、またはその両方で (例えば &quot;1, 2, 3, 5-10&quot; や &quot;1 2 3 5-10&quot;) 区切る必要があります。&lt;/li&gt;&lt;li&gt;すべてのページとページ間隔は、1 から PDF ファイルのページ数の間でなければなりません。&lt;/li&gt;&lt;li&gt;数字、スペース、カンマ、ダッシュのみ使用できます。他の文字は使用できません&lt;/li&gt;&lt;/ul&gt;</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="635"/>
        <location filename="../src/mainwindow.cpp" line="803"/>
        <source>Save PDF file</source>
        <translation>PDF ファイルを保存</translation>
    </message>
</context>
<context>
    <name>MultipageProfilesManager</name>
    <message>
        <location filename="../src/multipageprofilesmanager.cpp" line="34"/>
        <source>New profile…</source>
        <translation>新しいプロファイル…</translation>
    </message>
    <message>
        <location filename="../src/multipageprofilesmanager.cpp" line="38"/>
        <source>Delete profile</source>
        <translation>プロファイルを削除</translation>
    </message>
    <message>
        <location filename="../src/multipageprofilesmanager.cpp" line="46"/>
        <source>Manage multipage profiles</source>
        <translation>マルチページプロファイルの管理</translation>
    </message>
    <message>
        <location filename="../src/multipageprofilesmanager.cpp" line="79"/>
        <source>Edit profile</source>
        <translation>プロファイルを編集</translation>
    </message>
    <message>
        <location filename="../src/multipageprofilesmanager.cpp" line="125"/>
        <location filename="../src/multipageprofilesmanager.cpp" line="127"/>
        <source>Custom profile</source>
        <translation>カスタムプロファイル</translation>
    </message>
    <message>
        <location filename="../src/multipageprofilesmanager.cpp" line="186"/>
        <location filename="../src/multipageprofilesmanager.cpp" line="195"/>
        <location filename="../src/multipageprofilesmanager.cpp" line="209"/>
        <source>Error</source>
        <translation>エラー</translation>
    </message>
    <message>
        <location filename="../src/multipageprofilesmanager.cpp" line="187"/>
        <source>Profile name can not be empty.</source>
        <translation>プロファイル名は空にできません。</translation>
    </message>
    <message>
        <location filename="../src/multipageprofilesmanager.cpp" line="192"/>
        <source>Disabled</source>
        <translation>無効</translation>
    </message>
    <message>
        <location filename="../src/multipageprofilesmanager.cpp" line="196"/>
        <location filename="../src/multipageprofilesmanager.cpp" line="210"/>
        <source>Profile name already exists.</source>
        <translation>プロファイル名はすでに存在します。</translation>
    </message>
</context>
<context>
    <name>PdfInfoLabel</name>
    <message>
        <location filename="../src/pdfinfolabel.cpp" line="52"/>
        <source>portrait</source>
        <translation>縦向き</translation>
    </message>
    <message>
        <location filename="../src/pdfinfolabel.cpp" line="53"/>
        <source>landscape</source>
        <translation>横向き</translation>
    </message>
    <message numerus="yes">
        <location filename="../src/pdfinfolabel.cpp" line="63"/>
        <source>%n page(s)</source>
        <translation>
            <numerusform>%nページ</numerusform>
        </translation>
    </message>
</context>
</TS>
