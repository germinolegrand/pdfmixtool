<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="id">
<context>
    <name>AboutDialog</name>
    <message>
        <location filename="../src/aboutdialog.cpp" line="33"/>
        <source>About PDF Mix Tool</source>
        <translation>Tentang PDF Mix Tool</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="44"/>
        <source>Close</source>
        <translation>Tutup</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="55"/>
        <source>Version %1</source>
        <translation>Version %1</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="67"/>
        <source>An application to split, merge, rotate and mix PDF files.</source>
        <translation>Sebuah aplikasi untuk memisahkan, menyatukan, memutar dan mencampurkan berkas PDF.</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="70"/>
        <source>Website</source>
        <translation>Situs web</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="77"/>
        <source>About</source>
        <translation>Tentang</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="94"/>
        <source>Authors</source>
        <translation>Pencipta</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="96"/>
        <source>Translators</source>
        <translation>Penerjemah</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="105"/>
        <source>Credits</source>
        <translation>Kredit</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="129"/>
        <source>License</source>
        <translation>Lisensi</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="142"/>
        <source>Submit a pull request</source>
        <translation>Kirim pull request</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="143"/>
        <source>Report a bug</source>
        <translation>Laporkan bug</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="144"/>
        <source>Help translating</source>
        <translation>Bantu menerjemahkan</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="151"/>
        <source>Contribute</source>
        <translation>Berkontribusi</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="168"/>
        <source>Changelog</source>
        <translation>Catatan Perubahan</translation>
    </message>
</context>
<context>
    <name>AddEmptyPages</name>
    <message>
        <location filename="../src/single_file_operations/add_empty_pages.cpp" line="40"/>
        <source>Count:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/add_empty_pages.cpp" line="44"/>
        <source>Page size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/add_empty_pages.cpp" line="47"/>
        <source>Same as document</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/add_empty_pages.cpp" line="50"/>
        <source>Custom:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/add_empty_pages.cpp" line="75"/>
        <source>Standard:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/add_empty_pages.cpp" line="84"/>
        <source>Portrait</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/add_empty_pages.cpp" line="86"/>
        <source>Landscape</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/add_empty_pages.cpp" line="89"/>
        <source>Location</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/add_empty_pages.cpp" line="92"/>
        <source>Before</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/add_empty_pages.cpp" line="94"/>
        <source>After</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/add_empty_pages.cpp" line="97"/>
        <source>Page:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/add_empty_pages.cpp" line="111"/>
        <source>Save</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/add_empty_pages.cpp" line="122"/>
        <source>Save as…</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Booklet</name>
    <message>
        <location filename="../src/single_file_operations/booklet.cpp" line="34"/>
        <source>Left</source>
        <translation>Kiri</translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/booklet.cpp" line="35"/>
        <source>Right</source>
        <translation>Kanan</translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/booklet.cpp" line="36"/>
        <source>Binding:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/booklet.cpp" line="41"/>
        <source>Generate booklet</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DeletePages</name>
    <message>
        <location filename="../src/single_file_operations/delete_pages.cpp" line="41"/>
        <source>Delete pages:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/delete_pages.cpp" line="43"/>
        <source>Delete even pages</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/delete_pages.cpp" line="45"/>
        <source>Delete odd pages</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/delete_pages.cpp" line="61"/>
        <source>Save</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/delete_pages.cpp" line="72"/>
        <source>Save as…</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/delete_pages.cpp" line="153"/>
        <source>&lt;p&gt;Pages to be deleted are badly formatted. Please make sure you complied with the following rules:&lt;/p&gt;&lt;ul&gt;&lt;li&gt;intervals of pages must be written indicating the first page and the last page separated by a dash (e.g. &quot;1-5&quot;);&lt;/li&gt;&lt;li&gt;single pages and intervals of pages must be separated by spaces, commas or both (e.g. &quot;1, 2, 3, 5-10&quot; or &quot;1 2 3 5-10&quot;);&lt;/li&gt;&lt;li&gt;all pages and intervals of pages must be between 1 and the number of pages of the PDF file;&lt;/li&gt;&lt;li&gt;only numbers, spaces, commas and dashes can be used. All other characters are not allowed.&lt;/li&gt;&lt;/ul&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/delete_pages.cpp" line="168"/>
        <source>Error</source>
        <translation type="unfinished">Eror</translation>
    </message>
</context>
<context>
    <name>EditMultipageProfileDialog</name>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="29"/>
        <source>Edit multipage profile</source>
        <translation>Sunting profil multi halaman</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="64"/>
        <location filename="../src/editmultipageprofiledialog.cpp" line="166"/>
        <source>Left</source>
        <translation>Kiri</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="65"/>
        <location filename="../src/editmultipageprofiledialog.cpp" line="69"/>
        <source>Center</source>
        <translation>Tengah</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="66"/>
        <location filename="../src/editmultipageprofiledialog.cpp" line="168"/>
        <source>Right</source>
        <translation>Kanan</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="68"/>
        <location filename="../src/editmultipageprofiledialog.cpp" line="170"/>
        <source>Top</source>
        <translation>Atas</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="70"/>
        <location filename="../src/editmultipageprofiledialog.cpp" line="172"/>
        <source>Bottom</source>
        <translation>Bawah</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="101"/>
        <source>Name:</source>
        <translation>Nama:</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="109"/>
        <source>Output page size</source>
        <translation>Ukuran halaman keluaran</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="115"/>
        <source>Custom size:</source>
        <translation>Ukuran kustom:</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="129"/>
        <source>Pages layout</source>
        <translation>Tata letak halaman</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="132"/>
        <source>Rows:</source>
        <translation>Baris:</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="135"/>
        <source>Columns:</source>
        <translation>Kolom:</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="138"/>
        <source>Rotation:</source>
        <translation>Rotasi:</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="141"/>
        <source>Spacing:</source>
        <translation>Jarak:</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="149"/>
        <source>Pages alignment</source>
        <translation>Perataan halaman</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="152"/>
        <source>Horizontal:</source>
        <translation>Horizontal:</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="155"/>
        <source>Vertical:</source>
        <translation>Vertikal:</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="163"/>
        <source>Margins</source>
        <translation>Margin</translation>
    </message>
</context>
<context>
    <name>EditPageLayout</name>
    <message>
        <location filename="../src/single_file_operations/edit_page_layout.cpp" line="49"/>
        <source>No rotation</source>
        <translation type="unfinished">Tidak ada rotasi</translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/edit_page_layout.cpp" line="56"/>
        <source>Rotation:</source>
        <translation type="unfinished">Rotasi:</translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/edit_page_layout.cpp" line="58"/>
        <location filename="../src/single_file_operations/edit_page_layout.cpp" line="121"/>
        <location filename="../src/single_file_operations/edit_page_layout.cpp" line="208"/>
        <source>Disabled</source>
        <translation type="unfinished">Nonaktifkan</translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/edit_page_layout.cpp" line="66"/>
        <location filename="../src/single_file_operations/edit_page_layout.cpp" line="133"/>
        <location filename="../src/single_file_operations/edit_page_layout.cpp" line="216"/>
        <source>New custom profile…</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/edit_page_layout.cpp" line="72"/>
        <source>Multipage:</source>
        <translation type="unfinished">Multi halaman:</translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/edit_page_layout.cpp" line="80"/>
        <source>Scale page:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/edit_page_layout.cpp" line="88"/>
        <source>Save</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/edit_page_layout.cpp" line="101"/>
        <source>Save as…</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>EditPdfEntryDialog</name>
    <message>
        <location filename="../src/editpdfentrydialog.cpp" line="37"/>
        <source>Edit PDF files&apos; properties</source>
        <translation>Sunting properti berkas PDF</translation>
    </message>
    <message>
        <location filename="../src/editpdfentrydialog.cpp" line="40"/>
        <source>No rotation</source>
        <translation>Tidak ada rotasi</translation>
    </message>
    <message>
        <location filename="../src/editpdfentrydialog.cpp" line="45"/>
        <source>Disabled</source>
        <translation>Nonaktifkan</translation>
    </message>
    <message>
        <location filename="../src/editpdfentrydialog.cpp" line="90"/>
        <source>OK</source>
        <translation>OKE</translation>
    </message>
    <message>
        <location filename="../src/editpdfentrydialog.cpp" line="96"/>
        <source>Cancel</source>
        <translation>Batal</translation>
    </message>
    <message>
        <location filename="../src/editpdfentrydialog.cpp" line="103"/>
        <source>Multipage:</source>
        <translation>Multi halaman:</translation>
    </message>
    <message>
        <location filename="../src/editpdfentrydialog.cpp" line="105"/>
        <source>Rotation:</source>
        <translation>Rotasi:</translation>
    </message>
</context>
<context>
    <name>ExtractPages</name>
    <message>
        <location filename="../src/single_file_operations/extract_pages.cpp" line="44"/>
        <source>Extract pages:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/extract_pages.cpp" line="47"/>
        <source>Extract all pages</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/extract_pages.cpp" line="50"/>
        <source>Extract even pages</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/extract_pages.cpp" line="53"/>
        <source>Extract odd pages</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/extract_pages.cpp" line="60"/>
        <source>Output PDF base name:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/extract_pages.cpp" line="73"/>
        <source>Extract to individual PDF files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/extract_pages.cpp" line="77"/>
        <source>Extract to single PDF</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/extract_pages.cpp" line="145"/>
        <source>&lt;p&gt;Pages to be extracted are badly formatted. Please make sure you complied with the following rules:&lt;/p&gt;&lt;ul&gt;&lt;li&gt;intervals of pages must be written indicating the first page and the last page separated by a dash (e.g. &quot;1-5&quot;);&lt;/li&gt;&lt;li&gt;single pages and intervals of pages must be separated by spaces, commas or both (e.g. &quot;1, 2, 3, 5-10&quot; or &quot;1 2 3 5-10&quot;);&lt;/li&gt;&lt;li&gt;all pages and intervals of pages must be between 1 and the number of pages of the PDF file;&lt;/li&gt;&lt;li&gt;only numbers, spaces, commas and dashes can be used. All other characters are not allowed.&lt;/li&gt;&lt;/ul&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/extract_pages.cpp" line="160"/>
        <source>Error</source>
        <translation type="unfinished">Eror</translation>
    </message>
</context>
<context>
    <name>InputPdfFileDelegate</name>
    <message>
        <location filename="../src/inputpdffiledelegate.cpp" line="113"/>
        <source>All</source>
        <translation>Semua</translation>
    </message>
    <message>
        <location filename="../src/inputpdffiledelegate.cpp" line="80"/>
        <source>Page order:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/inputpdffiledelegate.cpp" line="83"/>
        <source>reverse</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/inputpdffiledelegate.cpp" line="85"/>
        <source>forward</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/inputpdffiledelegate.cpp" line="126"/>
        <source>Pages:</source>
        <translation>Halaman:</translation>
    </message>
    <message>
        <location filename="../src/inputpdffiledelegate.cpp" line="129"/>
        <source>Multipage:</source>
        <translation>Multi halaman:</translation>
    </message>
    <message>
        <location filename="../src/inputpdffiledelegate.cpp" line="134"/>
        <source>Disabled</source>
        <translation>Nonaktifkan</translation>
    </message>
    <message>
        <location filename="../src/inputpdffiledelegate.cpp" line="137"/>
        <source>Rotation:</source>
        <translation>Rotasi:</translation>
    </message>
    <message>
        <location filename="../src/inputpdffiledelegate.cpp" line="140"/>
        <source>Outline entry:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>InputPdfFileWidget</name>
    <message>
        <location filename="../src/inputpdffilewidget.cpp" line="57"/>
        <source>Reverse page order:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/inputpdffilewidget.cpp" line="71"/>
        <source>Disabled</source>
        <translation>Nonaktifkan</translation>
    </message>
    <message>
        <location filename="../src/inputpdffilewidget.cpp" line="79"/>
        <source>New custom profile…</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/inputpdffilewidget.cpp" line="81"/>
        <source>No rotation</source>
        <translation>Tidak ada rotasi</translation>
    </message>
    <message>
        <location filename="../src/inputpdffilewidget.cpp" line="86"/>
        <source>Pages:</source>
        <translation>Halaman:</translation>
    </message>
    <message>
        <location filename="../src/inputpdffilewidget.cpp" line="88"/>
        <source>Multipage:</source>
        <translation>Multi halaman:</translation>
    </message>
    <message>
        <location filename="../src/inputpdffilewidget.cpp" line="90"/>
        <source>Rotation:</source>
        <translation>Rotasi:</translation>
    </message>
    <message>
        <location filename="../src/inputpdffilewidget.cpp" line="92"/>
        <source>Outline entry:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../src/mainwindow.cpp" line="173"/>
        <source>Add PDF file</source>
        <translation>Tambahkan berkas PDF</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="178"/>
        <source>Move up</source>
        <translation>Pindah ke atas</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="183"/>
        <source>Move down</source>
        <translation>Pindah ke bawah</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="188"/>
        <source>Remove file</source>
        <translation>Hapus berkas</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="126"/>
        <source>About</source>
        <translation>Tentang</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="221"/>
        <location filename="../src/mainwindow.cpp" line="225"/>
        <source>Generate PDF</source>
        <translation>Menggenerasi PDF</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="307"/>
        <source>Edit page layout</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="628"/>
        <source>PDF generation error</source>
        <translation>gagal menggenerasi PDF</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="376"/>
        <source>Select one or more PDF files to open</source>
        <translation>Pilih satu atau lebih berkas PDF untuk dibuka</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="162"/>
        <source>Edit</source>
        <translation>Sunting</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="163"/>
        <source>View</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="166"/>
        <source>Main toolbar</source>
        <translation>Toolbar utama</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="116"/>
        <source>Menu</source>
        <translation>Menu</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="97"/>
        <source>Multiple files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="98"/>
        <source>Single file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="121"/>
        <source>Multipage profiles…</source>
        <translation>Profil multi halaman…</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="131"/>
        <source>Exit</source>
        <translation>Keluar</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="278"/>
        <source>Open PDF file…</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="302"/>
        <source>Booklet</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="784"/>
        <source>Always overwrite</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="939"/>
        <source>Select save directory</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1002"/>
        <source>Extract to single PDF</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="329"/>
        <source>Add empty pages</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="336"/>
        <source>Delete pages</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="343"/>
        <source>Extract pages</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="378"/>
        <location filename="../src/mainwindow.cpp" line="639"/>
        <location filename="../src/mainwindow.cpp" line="705"/>
        <location filename="../src/mainwindow.cpp" line="742"/>
        <location filename="../src/mainwindow.cpp" line="807"/>
        <location filename="../src/mainwindow.cpp" line="1006"/>
        <source>PDF files (*.pdf)</source>
        <translation>Berkas PDF (*.pdf)</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="602"/>
        <source>Output pages: %1</source>
        <translation>Halaman keluaran: %1</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="703"/>
        <source>Select a PDF file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="738"/>
        <source>Save booklet PDF file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="777"/>
        <source>Overwrite File?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="778"/>
        <source>A file called «%1» already exists. Do you want to overwrite it?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="611"/>
        <source>&lt;p&gt;Output pages of file &lt;b&gt;%1&lt;/b&gt; are badly formatted. Please make sure you complied with the following rules:&lt;/p&gt;&lt;ul&gt;&lt;li&gt;intervals of pages must be written indicating the first page and the last page separated by a dash (e.g. &quot;1-5&quot;);&lt;/li&gt;&lt;li&gt;single pages and intervals of pages must be separated by spaces, commas or both (e.g. &quot;1, 2, 3, 5-10&quot; or &quot;1 2 3 5-10&quot;);&lt;/li&gt;&lt;li&gt;all pages and intervals of pages must be between 1 and the number of pages of the PDF file;&lt;/li&gt;&lt;li&gt;only numbers, spaces, commas and dashes can be used. All other characters are not allowed.&lt;/li&gt;&lt;/ul&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="635"/>
        <location filename="../src/mainwindow.cpp" line="803"/>
        <source>Save PDF file</source>
        <translation>Simpan berkas PDF</translation>
    </message>
</context>
<context>
    <name>MultipageProfilesManager</name>
    <message>
        <location filename="../src/multipageprofilesmanager.cpp" line="34"/>
        <source>New profile…</source>
        <translation>Profil baru…</translation>
    </message>
    <message>
        <location filename="../src/multipageprofilesmanager.cpp" line="38"/>
        <source>Delete profile</source>
        <translation>Hapus profil</translation>
    </message>
    <message>
        <location filename="../src/multipageprofilesmanager.cpp" line="46"/>
        <source>Manage multipage profiles</source>
        <translation>Kelola profil multi halaman</translation>
    </message>
    <message>
        <location filename="../src/multipageprofilesmanager.cpp" line="79"/>
        <source>Edit profile</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/multipageprofilesmanager.cpp" line="125"/>
        <location filename="../src/multipageprofilesmanager.cpp" line="127"/>
        <source>Custom profile</source>
        <translation>Profil kustom</translation>
    </message>
    <message>
        <location filename="../src/multipageprofilesmanager.cpp" line="186"/>
        <location filename="../src/multipageprofilesmanager.cpp" line="195"/>
        <location filename="../src/multipageprofilesmanager.cpp" line="209"/>
        <source>Error</source>
        <translation>Eror</translation>
    </message>
    <message>
        <location filename="../src/multipageprofilesmanager.cpp" line="187"/>
        <source>Profile name can not be empty.</source>
        <translation>Nama profil tidak boleh kosong.</translation>
    </message>
    <message>
        <location filename="../src/multipageprofilesmanager.cpp" line="192"/>
        <source>Disabled</source>
        <translation>Nonaktifkan</translation>
    </message>
    <message>
        <location filename="../src/multipageprofilesmanager.cpp" line="196"/>
        <location filename="../src/multipageprofilesmanager.cpp" line="210"/>
        <source>Profile name already exists.</source>
        <translation>Nama profil sudah ada.</translation>
    </message>
</context>
<context>
    <name>PdfInfoLabel</name>
    <message>
        <location filename="../src/pdfinfolabel.cpp" line="52"/>
        <source>portrait</source>
        <translation>portrait</translation>
    </message>
    <message>
        <location filename="../src/pdfinfolabel.cpp" line="53"/>
        <source>landscape</source>
        <translation>landscape</translation>
    </message>
    <message numerus="yes">
        <location filename="../src/pdfinfolabel.cpp" line="63"/>
        <source>%n page(s)</source>
        <translation type="unfinished">
            <numerusform></numerusform>
        </translation>
    </message>
</context>
</TS>
