<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="tr">
<context>
    <name>AboutDialog</name>
    <message>
        <location filename="../src/aboutdialog.cpp" line="33"/>
        <source>About PDF Mix Tool</source>
        <translation>PDF Mix Tool Hakkında</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="44"/>
        <source>Close</source>
        <translation>Kapat</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="55"/>
        <source>Version %1</source>
        <translation>Sürüm %1</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="67"/>
        <source>An application to split, merge, rotate and mix PDF files.</source>
        <translation>PDF dosyalarını bölme, birleştirme, döndürme ve karıştırma yapan bir uygulama.</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="70"/>
        <source>Website</source>
        <translation>Web sitesi</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="77"/>
        <source>About</source>
        <translation>Hakkında</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="94"/>
        <source>Authors</source>
        <translation>Yazarlar</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="96"/>
        <source>Translators</source>
        <translation>Çevirmenler</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="105"/>
        <source>Credits</source>
        <translation>Emeği geçenler</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="129"/>
        <source>License</source>
        <translation>Lisans</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="142"/>
        <source>Submit a pull request</source>
        <translation>Çekme isteği gönderin</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="143"/>
        <source>Report a bug</source>
        <translation>Hata bildir</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="144"/>
        <source>Help translating</source>
        <translation>Çeviri yardımı</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="151"/>
        <source>Contribute</source>
        <translation>Katkıda bulun</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="168"/>
        <source>Changelog</source>
        <translation>Değişiklikler</translation>
    </message>
</context>
<context>
    <name>AddEmptyPages</name>
    <message>
        <location filename="../src/single_file_operations/add_empty_pages.cpp" line="40"/>
        <source>Count:</source>
        <translation>Sayı:</translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/add_empty_pages.cpp" line="44"/>
        <source>Page size</source>
        <translation>Sayfa boyutu</translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/add_empty_pages.cpp" line="47"/>
        <source>Same as document</source>
        <translation>Belge kadar</translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/add_empty_pages.cpp" line="50"/>
        <source>Custom:</source>
        <translation>Özel:</translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/add_empty_pages.cpp" line="75"/>
        <source>Standard:</source>
        <translation>Standart:</translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/add_empty_pages.cpp" line="84"/>
        <source>Portrait</source>
        <translation>Dikey</translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/add_empty_pages.cpp" line="86"/>
        <source>Landscape</source>
        <translation>Yatay</translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/add_empty_pages.cpp" line="89"/>
        <source>Location</source>
        <translation>Konum</translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/add_empty_pages.cpp" line="92"/>
        <source>Before</source>
        <translation>Önce</translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/add_empty_pages.cpp" line="94"/>
        <source>After</source>
        <translation>Sonra</translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/add_empty_pages.cpp" line="97"/>
        <source>Page:</source>
        <translation>Sayfa:</translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/add_empty_pages.cpp" line="111"/>
        <source>Save</source>
        <translation>Kaydet</translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/add_empty_pages.cpp" line="122"/>
        <source>Save as…</source>
        <translation>Farklı kaydet…</translation>
    </message>
</context>
<context>
    <name>Booklet</name>
    <message>
        <location filename="../src/single_file_operations/booklet.cpp" line="34"/>
        <source>Left</source>
        <translation>Sol</translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/booklet.cpp" line="35"/>
        <source>Right</source>
        <translation>Sağ</translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/booklet.cpp" line="36"/>
        <source>Binding:</source>
        <translation>Cilt:</translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/booklet.cpp" line="41"/>
        <source>Generate booklet</source>
        <translation>Kitapçık oluştur</translation>
    </message>
</context>
<context>
    <name>DeletePages</name>
    <message>
        <location filename="../src/single_file_operations/delete_pages.cpp" line="41"/>
        <source>Delete pages:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/delete_pages.cpp" line="43"/>
        <source>Delete even pages</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/delete_pages.cpp" line="45"/>
        <source>Delete odd pages</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/delete_pages.cpp" line="61"/>
        <source>Save</source>
        <translation type="unfinished">Kaydet</translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/delete_pages.cpp" line="72"/>
        <source>Save as…</source>
        <translation type="unfinished">Farklı kaydet…</translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/delete_pages.cpp" line="153"/>
        <source>&lt;p&gt;Pages to be deleted are badly formatted. Please make sure you complied with the following rules:&lt;/p&gt;&lt;ul&gt;&lt;li&gt;intervals of pages must be written indicating the first page and the last page separated by a dash (e.g. &quot;1-5&quot;);&lt;/li&gt;&lt;li&gt;single pages and intervals of pages must be separated by spaces, commas or both (e.g. &quot;1, 2, 3, 5-10&quot; or &quot;1 2 3 5-10&quot;);&lt;/li&gt;&lt;li&gt;all pages and intervals of pages must be between 1 and the number of pages of the PDF file;&lt;/li&gt;&lt;li&gt;only numbers, spaces, commas and dashes can be used. All other characters are not allowed.&lt;/li&gt;&lt;/ul&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/delete_pages.cpp" line="168"/>
        <source>Error</source>
        <translation type="unfinished">Hata</translation>
    </message>
</context>
<context>
    <name>EditMultipageProfileDialog</name>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="29"/>
        <source>Edit multipage profile</source>
        <translation>Çok sayfalı profili düzenle</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="64"/>
        <location filename="../src/editmultipageprofiledialog.cpp" line="166"/>
        <source>Left</source>
        <translation>Sol</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="65"/>
        <location filename="../src/editmultipageprofiledialog.cpp" line="69"/>
        <source>Center</source>
        <translation>Merkez</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="66"/>
        <location filename="../src/editmultipageprofiledialog.cpp" line="168"/>
        <source>Right</source>
        <translation>Sağ</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="68"/>
        <location filename="../src/editmultipageprofiledialog.cpp" line="170"/>
        <source>Top</source>
        <translation>Üst</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="70"/>
        <location filename="../src/editmultipageprofiledialog.cpp" line="172"/>
        <source>Bottom</source>
        <translation>Alt</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="101"/>
        <source>Name:</source>
        <translation>Adı:</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="109"/>
        <source>Output page size</source>
        <translation>Çıktı sayfa boyutu</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="115"/>
        <source>Custom size:</source>
        <translation>Özel boyut:</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="129"/>
        <source>Pages layout</source>
        <translation>Sayfa düzeni</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="132"/>
        <source>Rows:</source>
        <translation>Satırlar:</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="135"/>
        <source>Columns:</source>
        <translation>Sütunlar:</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="138"/>
        <source>Rotation:</source>
        <translation>Döndürme:</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="141"/>
        <source>Spacing:</source>
        <translation>Aralık:</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="149"/>
        <source>Pages alignment</source>
        <translation>Sayfa hizalama</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="152"/>
        <source>Horizontal:</source>
        <translation>Yatay:</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="155"/>
        <source>Vertical:</source>
        <translation>Dikey:</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="163"/>
        <source>Margins</source>
        <translation>Kenar boşlukları</translation>
    </message>
</context>
<context>
    <name>EditPageLayout</name>
    <message>
        <location filename="../src/single_file_operations/edit_page_layout.cpp" line="49"/>
        <source>No rotation</source>
        <translation type="unfinished">Döndürme</translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/edit_page_layout.cpp" line="56"/>
        <source>Rotation:</source>
        <translation type="unfinished">Döndürme:</translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/edit_page_layout.cpp" line="58"/>
        <location filename="../src/single_file_operations/edit_page_layout.cpp" line="121"/>
        <location filename="../src/single_file_operations/edit_page_layout.cpp" line="208"/>
        <source>Disabled</source>
        <translation type="unfinished">Devre dışı</translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/edit_page_layout.cpp" line="66"/>
        <location filename="../src/single_file_operations/edit_page_layout.cpp" line="133"/>
        <location filename="../src/single_file_operations/edit_page_layout.cpp" line="216"/>
        <source>New custom profile…</source>
        <translation type="unfinished">Yeni özel profil…</translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/edit_page_layout.cpp" line="72"/>
        <source>Multipage:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/edit_page_layout.cpp" line="80"/>
        <source>Scale page:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/edit_page_layout.cpp" line="88"/>
        <source>Save</source>
        <translation type="unfinished">Kaydet</translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/edit_page_layout.cpp" line="101"/>
        <source>Save as…</source>
        <translation type="unfinished">Farklı kaydet…</translation>
    </message>
</context>
<context>
    <name>EditPdfEntryDialog</name>
    <message>
        <location filename="../src/editpdfentrydialog.cpp" line="37"/>
        <source>Edit PDF files&apos; properties</source>
        <translation>PDF dosyaların özelliklerini düzenle</translation>
    </message>
    <message>
        <location filename="../src/editpdfentrydialog.cpp" line="40"/>
        <source>No rotation</source>
        <translation>Döndürme</translation>
    </message>
    <message>
        <location filename="../src/editpdfentrydialog.cpp" line="45"/>
        <source>Disabled</source>
        <translation>Devre dışı</translation>
    </message>
    <message>
        <location filename="../src/editpdfentrydialog.cpp" line="90"/>
        <source>OK</source>
        <translation>Tamam</translation>
    </message>
    <message>
        <location filename="../src/editpdfentrydialog.cpp" line="96"/>
        <source>Cancel</source>
        <translation>İptal</translation>
    </message>
    <message>
        <location filename="../src/editpdfentrydialog.cpp" line="103"/>
        <source>Multipage:</source>
        <translation>Çok sayfalı:</translation>
    </message>
    <message>
        <location filename="../src/editpdfentrydialog.cpp" line="105"/>
        <source>Rotation:</source>
        <translation>Döndürme:</translation>
    </message>
</context>
<context>
    <name>ExtractPages</name>
    <message>
        <location filename="../src/single_file_operations/extract_pages.cpp" line="44"/>
        <source>Extract pages:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/extract_pages.cpp" line="47"/>
        <source>Extract all pages</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/extract_pages.cpp" line="50"/>
        <source>Extract even pages</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/extract_pages.cpp" line="53"/>
        <source>Extract odd pages</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/extract_pages.cpp" line="60"/>
        <source>Output PDF base name:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/extract_pages.cpp" line="73"/>
        <source>Extract to individual PDF files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/extract_pages.cpp" line="77"/>
        <source>Extract to single PDF</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/extract_pages.cpp" line="145"/>
        <source>&lt;p&gt;Pages to be extracted are badly formatted. Please make sure you complied with the following rules:&lt;/p&gt;&lt;ul&gt;&lt;li&gt;intervals of pages must be written indicating the first page and the last page separated by a dash (e.g. &quot;1-5&quot;);&lt;/li&gt;&lt;li&gt;single pages and intervals of pages must be separated by spaces, commas or both (e.g. &quot;1, 2, 3, 5-10&quot; or &quot;1 2 3 5-10&quot;);&lt;/li&gt;&lt;li&gt;all pages and intervals of pages must be between 1 and the number of pages of the PDF file;&lt;/li&gt;&lt;li&gt;only numbers, spaces, commas and dashes can be used. All other characters are not allowed.&lt;/li&gt;&lt;/ul&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/extract_pages.cpp" line="160"/>
        <source>Error</source>
        <translation type="unfinished">Hata</translation>
    </message>
</context>
<context>
    <name>InputPdfFileDelegate</name>
    <message>
        <location filename="../src/inputpdffiledelegate.cpp" line="113"/>
        <source>All</source>
        <translation>Tüm</translation>
    </message>
    <message>
        <location filename="../src/inputpdffiledelegate.cpp" line="80"/>
        <source>Page order:</source>
        <translation>Sayfa sıralaması:</translation>
    </message>
    <message>
        <location filename="../src/inputpdffiledelegate.cpp" line="83"/>
        <source>reverse</source>
        <translation>ters</translation>
    </message>
    <message>
        <location filename="../src/inputpdffiledelegate.cpp" line="85"/>
        <source>forward</source>
        <translation>ileri</translation>
    </message>
    <message>
        <location filename="../src/inputpdffiledelegate.cpp" line="126"/>
        <source>Pages:</source>
        <translation>Sayfalar:</translation>
    </message>
    <message>
        <location filename="../src/inputpdffiledelegate.cpp" line="129"/>
        <source>Multipage:</source>
        <translation>Çoklu sayfa:</translation>
    </message>
    <message>
        <location filename="../src/inputpdffiledelegate.cpp" line="134"/>
        <source>Disabled</source>
        <translation>Devre dışı</translation>
    </message>
    <message>
        <location filename="../src/inputpdffiledelegate.cpp" line="137"/>
        <source>Rotation:</source>
        <translation>Döndürme:</translation>
    </message>
    <message>
        <location filename="../src/inputpdffiledelegate.cpp" line="140"/>
        <source>Outline entry:</source>
        <translation>Anahat girişi:</translation>
    </message>
</context>
<context>
    <name>InputPdfFileWidget</name>
    <message>
        <location filename="../src/inputpdffilewidget.cpp" line="57"/>
        <source>Reverse page order:</source>
        <translation>Sayfa sıralamasını ters çevir:</translation>
    </message>
    <message>
        <location filename="../src/inputpdffilewidget.cpp" line="71"/>
        <source>Disabled</source>
        <translation>Devre dışı</translation>
    </message>
    <message>
        <location filename="../src/inputpdffilewidget.cpp" line="79"/>
        <source>New custom profile…</source>
        <translation>Yeni özel profil…</translation>
    </message>
    <message>
        <location filename="../src/inputpdffilewidget.cpp" line="81"/>
        <source>No rotation</source>
        <translation>Döndürme</translation>
    </message>
    <message>
        <location filename="../src/inputpdffilewidget.cpp" line="86"/>
        <source>Pages:</source>
        <translation>Sayfalar:</translation>
    </message>
    <message>
        <location filename="../src/inputpdffilewidget.cpp" line="88"/>
        <source>Multipage:</source>
        <translation>Çok sayfalı:</translation>
    </message>
    <message>
        <location filename="../src/inputpdffilewidget.cpp" line="90"/>
        <source>Rotation:</source>
        <translation>Döndürme:</translation>
    </message>
    <message>
        <location filename="../src/inputpdffilewidget.cpp" line="92"/>
        <source>Outline entry:</source>
        <translation>Anahat girişi:</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../src/mainwindow.cpp" line="173"/>
        <source>Add PDF file</source>
        <translation>PDF dosyası ekle</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="178"/>
        <source>Move up</source>
        <translation>Yukarı taşı</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="183"/>
        <source>Move down</source>
        <translation>Aşağı taşı</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="188"/>
        <source>Remove file</source>
        <translation>Dosyayı kaldır</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="126"/>
        <source>About</source>
        <translation>Hakkında</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="221"/>
        <location filename="../src/mainwindow.cpp" line="225"/>
        <source>Generate PDF</source>
        <translation>PDF oluştur</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="307"/>
        <source>Edit page layout</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="628"/>
        <source>PDF generation error</source>
        <translation>PDF oluşturma hatası</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="376"/>
        <source>Select one or more PDF files to open</source>
        <translation>Açmak için bir veya daha fazla PDF dosya seçin</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="162"/>
        <source>Edit</source>
        <translation>Düzenle</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="163"/>
        <source>View</source>
        <translation>Görünüm</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="166"/>
        <source>Main toolbar</source>
        <translation>Ana Araç Çubuğu</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="116"/>
        <source>Menu</source>
        <translation>Menü</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="97"/>
        <source>Multiple files</source>
        <translation>Çoklu dosya</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="98"/>
        <source>Single file</source>
        <translation>Tek dosya</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="121"/>
        <source>Multipage profiles…</source>
        <translation>Çok sayfalı profiller…</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="131"/>
        <source>Exit</source>
        <translation>Çıkış</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="278"/>
        <source>Open PDF file…</source>
        <translation>PDF dosya aç…</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="302"/>
        <source>Booklet</source>
        <translation>Kitapçık</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="784"/>
        <source>Always overwrite</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="939"/>
        <source>Select save directory</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1002"/>
        <source>Extract to single PDF</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="329"/>
        <source>Add empty pages</source>
        <translation>Boş sayfa ekle</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="336"/>
        <source>Delete pages</source>
        <translation>Sayfa sil</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="343"/>
        <source>Extract pages</source>
        <translation>Sayfa çıkar</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="378"/>
        <location filename="../src/mainwindow.cpp" line="639"/>
        <location filename="../src/mainwindow.cpp" line="705"/>
        <location filename="../src/mainwindow.cpp" line="742"/>
        <location filename="../src/mainwindow.cpp" line="807"/>
        <location filename="../src/mainwindow.cpp" line="1006"/>
        <source>PDF files (*.pdf)</source>
        <translation>PDF dosyalar (*.pdf)</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="602"/>
        <source>Output pages: %1</source>
        <translation>Çıktı sayfaları: %1</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="703"/>
        <source>Select a PDF file</source>
        <translation>PDF dosyası seç</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="738"/>
        <source>Save booklet PDF file</source>
        <translation>Kitapçık PDF dosyasını kaydet</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="777"/>
        <source>Overwrite File?</source>
        <translation>Dosyanın üzerine yazılsın mı?</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="778"/>
        <source>A file called «%1» already exists. Do you want to overwrite it?</source>
        <translation>«%1» adında bir dosya zaten var. Üzerine yazmak istiyor musunuz?</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="611"/>
        <source>&lt;p&gt;Output pages of file &lt;b&gt;%1&lt;/b&gt; are badly formatted. Please make sure you complied with the following rules:&lt;/p&gt;&lt;ul&gt;&lt;li&gt;intervals of pages must be written indicating the first page and the last page separated by a dash (e.g. &quot;1-5&quot;);&lt;/li&gt;&lt;li&gt;single pages and intervals of pages must be separated by spaces, commas or both (e.g. &quot;1, 2, 3, 5-10&quot; or &quot;1 2 3 5-10&quot;);&lt;/li&gt;&lt;li&gt;all pages and intervals of pages must be between 1 and the number of pages of the PDF file;&lt;/li&gt;&lt;li&gt;only numbers, spaces, commas and dashes can be used. All other characters are not allowed.&lt;/li&gt;&lt;/ul&gt;</source>
        <translation>&lt;p&gt;Dosya çıktı sayfaları %&lt;b&gt;%1&lt;/b&gt; kötü biçimlendirilmiş. Lütfen aşağıdaki kurallara uyduğunuzdan emin olun:&lt;/p&gt;&lt;ul&gt;&lt;li&gt;sayfa aralıkları, ilk sayfayı ve son sayfayı bir çizgiyle ayrılmış olarak belirterek yazılmalıdır (örn. &quot;1-5&quot;&lt;/li&gt;); &lt;li&gt;tek sayfalar ve sayfaların aralıkları boşluklar, virgüller veya her ikisi ile ayrılmalıdır (örn. &quot;1, 2, 3, 5-10&quot; veya &quot;1 2 3 5-10&quot;&lt;/li&gt;); &lt;li&gt;tüm sayfalar ve sayfa aralıkları 1 ile PDF dosyasının sayfa sayısı arasında olmalıdır;&lt;/li&gt; &lt;li&gt; sadece sayılar, boşluklar, virgüller ve tireler kullanılabilir. Diğer tüm karakterlere izin verilmiyor.&lt;/li&gt;&lt;/ul&gt;</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="635"/>
        <location filename="../src/mainwindow.cpp" line="803"/>
        <source>Save PDF file</source>
        <translation>PDF dosyayı kaydet</translation>
    </message>
</context>
<context>
    <name>MultipageProfilesManager</name>
    <message>
        <location filename="../src/multipageprofilesmanager.cpp" line="34"/>
        <source>New profile…</source>
        <translation>Yeni profil…</translation>
    </message>
    <message>
        <location filename="../src/multipageprofilesmanager.cpp" line="38"/>
        <source>Delete profile</source>
        <translation>Profili sil</translation>
    </message>
    <message>
        <location filename="../src/multipageprofilesmanager.cpp" line="46"/>
        <source>Manage multipage profiles</source>
        <translation>Çok sayfalı profilleri yönet</translation>
    </message>
    <message>
        <location filename="../src/multipageprofilesmanager.cpp" line="79"/>
        <source>Edit profile</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/multipageprofilesmanager.cpp" line="125"/>
        <location filename="../src/multipageprofilesmanager.cpp" line="127"/>
        <source>Custom profile</source>
        <translation>Özel profil</translation>
    </message>
    <message>
        <location filename="../src/multipageprofilesmanager.cpp" line="186"/>
        <location filename="../src/multipageprofilesmanager.cpp" line="195"/>
        <location filename="../src/multipageprofilesmanager.cpp" line="209"/>
        <source>Error</source>
        <translation>Hata</translation>
    </message>
    <message>
        <location filename="../src/multipageprofilesmanager.cpp" line="187"/>
        <source>Profile name can not be empty.</source>
        <translation>Profil adı boş olamaz.</translation>
    </message>
    <message>
        <location filename="../src/multipageprofilesmanager.cpp" line="192"/>
        <source>Disabled</source>
        <translation>Devre dışı</translation>
    </message>
    <message>
        <location filename="../src/multipageprofilesmanager.cpp" line="196"/>
        <location filename="../src/multipageprofilesmanager.cpp" line="210"/>
        <source>Profile name already exists.</source>
        <translation>Profil adı zaten var.</translation>
    </message>
</context>
<context>
    <name>PdfInfoLabel</name>
    <message>
        <location filename="../src/pdfinfolabel.cpp" line="52"/>
        <source>portrait</source>
        <translation>dikey</translation>
    </message>
    <message>
        <location filename="../src/pdfinfolabel.cpp" line="53"/>
        <source>landscape</source>
        <translation>yatay</translation>
    </message>
    <message numerus="yes">
        <location filename="../src/pdfinfolabel.cpp" line="63"/>
        <source>%n page(s)</source>
        <translation>
            <numerusform>%n sayfa</numerusform>
        </translation>
    </message>
</context>
</TS>
